export default {
  title: 'Gerencia de Informática',
  description: 'Sistemas de Fogade',
  base: '/', //  The default path during deployment / secondary address / base can be used/
  themeConfig: {
    logo: '/logo.png',
    nav: [
      { text: 'Inicio', link: '/' },      
      { text: 'Comenzar', link: '/guide/intro' },
      
      { text: 'GitLab', link: 'git@gitlab.com:imaldonado22/documentacion-constancia.git' }      
    ],
    sidebar: [
      {
        text: 'Tutorial',   // required
        path: '/guide/',      // optional, link of the title, which should be an absolute path and must exist        
        sidebarDepth: 1,    // optional, defaults to 1 
        items: [
          { text: 'Contexto', link: '/guide/intro' },
          { text: 'Documentacion Sistema', link: '/guide/documentation_system' },
          { text: 'La Sincronización', link: '/guide/synchronization' },
          { text: 'Requerimientos', link: '/guide/requirements' },
          { text: 'la firma', link: '/guide/business' },
          
         
      
        ]
      }
    ]
  }
}
